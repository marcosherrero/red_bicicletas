
require('dotenv').config();  //configuracion de variables de ambiente   (fichero .env)

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var newrelic = require('newrelic');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas'); //nuevo
var bicicletasAPIRouter = require('./routes/api/bicicletas'); //nuevo
var usuariosAPIRouter = require('./routes/api/usuarios'); //nuevo

var UsuariosRouter=require('./routes/usuarios')
var tokenRouter=require('./routes/token')

var passport = require("./config/passport");
const session = require("express-session");
const usuario = require("./models/usuario");
const Token = require("./models/token");
const jwt = require("jsonwebtoken");
var authAPIRouter = require("./routes/api/auth");

const MongoDBStore = require ('connect-mongodb-session')(session); //para guardar la sesion en mongodb

var app = express();
//passport(app)

app.set("secretKey", "jwt_pwd_!!223344");  //para descifrar los token de json

//const store=new session.MemoryStore;
let store;
if (process.env.MODE_ENV === 'development'){
	store=new session.MemoryStore;L
}else{
	store=new MongoDBStore(
		{
			uri: process.env.MONGO_URI,
			collection: 'sessions'
		}
	);
	store.on ('error', function (error){
		assert.ifError(error);
		assert.ok(false);
	});
};


app.use(
  session({
    cookie: {
      maxAge: 240 * 60 * 60 * 1000
    },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: 'red_bicis_!!!***."."."."."!!!!123123'
  })
);




var mongoose = require('mongoose');
var mongoDB = process.env.MONGO_URI; //--> registrado en variable de ambiente (fichero .env)
//var mongoDB = "mongodb+srv://Admin:KwMvn8IUthWcPwSt@red-bicicletas-lnjar.mongodb.net/test?retryWrites=true&w=majority"
mongoose.connect(mongoDB, { useNewUrlParser : true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connections Error'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get("/login", function (req, res) {
  res.render("session/login");
});

app.post("/login", function (req, res, next) {
  passport.authenticate("local", function (err, usuario, info) {
  	console.log("entra 1");
    if (err) {
    	console.log("entra 2");
    	return next(err);
    }
    if (!usuario) {
    	console.log("entra 3");
    	return res.render("session/login", {info});
    }
    console.log("entra 4");
    req.login(usuario, function (err) {
      if (err) {
      	console.log("entra 5");
      	return next(err);
      }
      return res.redirect("/");
    });
    console.log("entra 6");
  })(req, res, next);
});

app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});

app.get("/forgotPassword", function (req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function (req, res) {
  usuario.findOne({
    email: req.body.email
  }, function (err, usuario) {
    if (!usuario)
      return res.render("session/forgotPassword", {
        info: {
          message: "No existe el email para un usuario existente."
        }
      });

    usuario.resetPassword(function (err) {
      if (err) return next(err);
      console.log("session/forgotPasswordMessage");
    });

    res.render("session/forgotPasswordMessage");
  });
});
app.get("/resetPassword/:token", function (req, res, next) {
  Token.findOne({
    token: req.params.token
  }, function (err, token) {
    if (!token)
      return res.status(400).send({
        type: "not-verified",
        msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado."
      });

    usuario.findById(token._userId, function (err, usuario) {
      if (!usuario)
        return res
          .status(400)
          .send({
            msg: "No existe un usuario asociado al token."
          });
      res.render("session/resetPassword", {
        errors: {},
        usuario: usuario
      });
    });
  });
});

app.post("/resetPassword", function (req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPassword", {
      errors: {
        confirm_password: {
          message: "No coincide con el password ingresado"
        }
      },
      usuario: new usuario({
        email: req.body.email
      })
    });
    return;
  }
  usuario.findOne({
    email: req.body.email
  }, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (err) {
      if (err) {
        res.render("session/resetPassword", {
          errors: err.errors,
          usuario: new usuario({
            email: req.body.email
          })
        });
      } else {
        res.redirect("/login");
      }
    });
  });
});

app.post('/')
app.use('/', indexRouter);
app.use('/users',usersRouter);
app.use('/bicicletas' , loggedIn,bicicletasRouter); //nuevo
app.use('/api/bicicletas',validarUsuario, bicicletasAPIRouter); //nuevo  antes de entrar en bicicletas valida si el usuario existe
app.use('/api/usuarios', usuariosAPIRouter); //nuevo
app.use('/usuarios',loggedIn,UsuariosRouter);
app.use('/token',tokenRouter);
app.use('/api/auth', authAPIRouter);


//---- INICIO Seguridad OAuth
app.use("/privacy_policy", function (req, res) {
  res.sendFile("public/privacy_policy.html");
});

app.use("/googled0eff6e62ae3678c", function (req, res) {
  res.sendFile("public/googled0eff6e62ae3678c.html");
});

app.get("/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/plus.login",
      "https://www.googleapis.com/auth/plus.profile.emails.read",
      "profile",
      "email"
    ]
  })
);

app.get("/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/error"
  })
);

//---- FIN Seguridad OAuth


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("Usuario sin loguearse");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretKey"), function (
    err,
    decoded
  ) {
    if (err) {
      res.json({
        status: "error",
        message: err.message,
        data: null
      });
    } else {
      req.body.userId = decoded.id;

      console.log("jwt verify: " + decoded);

      next();
    }
  });
}
module.exports = app;
