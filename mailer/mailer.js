var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');


//PARA TRABAJAR ENVIANDO CORREOS EN LOCAL A UNA CUENTA DESCONOCIDA: ETHEREAL MAIL
/*
let mailConfig;
    // all emails are catched by ethereal.email
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        //user:'aaliyah.waelchi@ethereal.email',  //Estos datos los hemos conseguido tras registrarnos en ethernet email (mirar apuntes)
        //pass:'bmXr4cPHF2sfBEnftD'
        user: process.env.ethereal_user,
        pass: process.env.ethereal_password
      }
    };
*/

//PARA TRABAJAR EN PRODUCCION CON L902.626.004 LOS ENVIOS DE CORREOS USAREMOS NODEMAILER-SENDGRID-TRANSPORT ,CONFIGURAMOS LA CUENTA EN LA PAGINA DE SANDGRID
let mailConfig;

//ENTRAMOS EN MODO DE PRODUCCION
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {api_key: process.env.SENDGRID_API_SECRET}
    };
    mailConfig = sgTransport(options);
} else {
	if (process.env.NODE_ENV === 'starting') {
        const options = {
            auth: {api_key: process.env.SENDGRID_API_SECRET}
        };
        mailConfig = sgTransport(options);
  } else {
    		//ENTRAMOS EN MODO AMBIENTE DE DESARROLLO
        mailConfig = {
      		host: "smtp.ethereal.email",
      		port: 587,
      		auth: {
        		user:process.env.ethereal_user,
        		pass:process.env.ethereal_pwd
      		}
    		};
   }
}

module.exports = nodemailer.createTransport(mailConfig);
